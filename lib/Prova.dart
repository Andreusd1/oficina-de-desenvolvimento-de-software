import 'dart:io';

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:open_file_plus/open_file_plus.dart';
import 'package:path_provider/path_provider.dart';

class Prova extends StatefulWidget {
  final String id;
  final String disciplina;
  final String nome;

  const Prova(
      {super.key,
      required this.id,
      required this.nome,
      required this.disciplina});

  @override
  State<Prova> createState() => _ProvaState(id, nome, disciplina);
}

class _ProvaState extends State<Prova> {
  final String id;
  final String disciplina;
  final String nome;

  final storage = FirebaseStorage.instance;
  CollectionReference comentarios =
      FirebaseFirestore.instance.collection('comentarios');

  String? username;

  _ProvaState(this.id, this.disciplina, this.nome);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('$disciplina - $nome')),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            const SizedBox(
              height: 10,
            ),
            const Center(
              child: Text(
                "Arquivos",
                style: TextStyle(fontSize: 20),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Card(
              color: Colors.green,
              elevation: 10,
              margin: EdgeInsets.symmetric(vertical: 10),
              child: ListTile(
                leading: Icon(Icons.open_in_full),
                title: Text("Abrir prova"),
                onTap: () async {
                  var path = 'provas/$id.pdf';
                  var content = await storage.ref(path).getData();
                  final tempDir = await getTemporaryDirectory();
                  File file = await File('${tempDir.path}/$id.pdf').create();
                  file.writeAsBytesSync(content!);
                  OpenFile.open('${tempDir.path}/$id.pdf');
                },
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            const Center(
              child: Text(
                "Comentários",
                style: TextStyle(fontSize: 20),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Center(
              child: Text(
                username != null ? 'Logado como $username' : '',
                style: const TextStyle(fontSize: 10),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Flexible(
              child: StreamBuilder(
                stream: comentarios.snapshots(),
                builder: (BuildContext context, AsyncSnapshot streamSnapshot) {
                  if (streamSnapshot.hasData) {
                    List<DocumentSnapshot> documents =
                        streamSnapshot.data.docs.where((element) {
                      return element.get('prova').toString().trim() ==
                          id.trim();
                    }).toList();
                    var pontuacoes = <int>[];
                    for (var comentario in documents) {
                      final upvotes = comentario['upvotes'].length;
                      final downvotes = comentario['downvotes'].length;
                      pontuacoes.add(upvotes - downvotes);
                    }
                    documents.sort((a, b) {
                      final upvotesA = a['upvotes'].length;
                      final downvotesA = a['downvotes'].length;

                      final upvotesB = b['upvotes'].length;
                      final downvotesB = b['downvotes'].length;

                      return (upvotesB - downvotesB) - (upvotesA - downvotesA);
                    });
                    pontuacoes.sort((a, b) => b - a);
                    return ListView.builder(
                      itemCount: documents.length,
                      itemBuilder: (BuildContext context, int index) {
                        final DocumentSnapshot comentario = documents[index];
                        return Card(
                            color: Colors.yellow,
                            elevation: 10,
                            margin: const EdgeInsets.symmetric(vertical: 10),
                            child: ListTile(
                              trailing: Text(
                                pontuacoes[index].toString(),
                              ),
                              leading: Column(
                                children: [
                                  IconButton(
                                      color: comentario['upvotes']
                                              .contains(username)
                                          ? Colors.red
                                          : Colors.black45,
                                      padding: EdgeInsets.zero,
                                      constraints: const BoxConstraints(),
                                      onPressed: () async {
                                        if (username == null) {
                                          alerta(
                                              "Você deve estar logado para avaliar um comentário.");
                                        } else {
                                          var newUpvotes =
                                              List.of(comentario['upvotes']);
                                          if (newUpvotes.contains(username)) {
                                            newUpvotes.remove(username);
                                          } else {
                                            newUpvotes.add(username);
                                          }
                                          var newDownvotes =
                                              List.of(comentario['downvotes']);
                                          if (newDownvotes.contains(username)) {
                                            newDownvotes.remove(username);
                                          }
                                          await comentarios
                                              .doc(comentario.id)
                                              .update({
                                            'conteudo': comentario['conteudo'],
                                            'upvotes': newUpvotes,
                                            "downvotes": newDownvotes,
                                            "usuario": comentario['usuario'],
                                            "prova": comentario['prova']
                                          });
                                        }
                                      },
                                      icon: const Icon(Icons.arrow_upward)),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  IconButton(
                                      color: comentario['downvotes']
                                              .contains(username)
                                          ? Colors.red
                                          : Colors.black45,
                                      padding: EdgeInsets.zero,
                                      constraints: const BoxConstraints(),
                                      onPressed: () async {
                                        if (username == null) {
                                          alerta(
                                              "Você deve estar logado para avaliar um comentário.");
                                        } else {
                                          var newDownvotes =
                                              List.of(comentario['downvotes']);
                                          if (newDownvotes.contains(username)) {
                                            newDownvotes.remove(username);
                                          } else {
                                            newDownvotes.add(username);
                                          }
                                          var newUpvotes =
                                              List.of(comentario['upvotes']);
                                          if (newUpvotes.contains(username)) {
                                            newUpvotes.remove(username);
                                          }
                                          await comentarios
                                              .doc(comentario.id)
                                              .update({
                                            'conteudo': comentario['conteudo'],
                                            'downvotes': newDownvotes,
                                            "upvotes": newUpvotes,
                                            "usuario": comentario['usuario'],
                                            "prova": comentario['prova']
                                          });
                                        }
                                      },
                                      icon: const Icon(Icons.arrow_downward)),
                                ],
                              ),
                              title: Text(comentario['usuario']),
                              subtitle: Text(comentario['conteudo']),
                              onLongPress: () {
                                if (comentario['usuario'] == username) {
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return AlertDialog(
                                          title: const Text("Confirmação"),
                                          content: const Text(
                                              "Tem Certeza que deseja excluir seu comentário?"),
                                          actions: [
                                            ElevatedButton(
                                                onPressed: () {
                                                  Navigator.of(context,
                                                          rootNavigator: true)
                                                      .pop();
                                                },
                                                child: const Text("Cancelar")),
                                            ElevatedButton(
                                                onPressed: () async {
                                                  await comentarios
                                                      .doc(comentario.id)
                                                      .delete();
                                                  Navigator.of(context,
                                                          rootNavigator: true)
                                                      .pop();
                                                },
                                                child: const Text("Confirmar"))
                                          ],
                                        );
                                      });
                                }
                              },
                            ));
                      },
                    );
                  } else {
                    return CircularProgressIndicator();
                  }
                },
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: Wrap(
        direction: Axis.horizontal,
        children: [
          FloatingActionButton(
              heroTag: "btn1",
              backgroundColor: Colors.green,
              child: const Icon(Icons.person),
              onPressed: () async {
                var usernameInput = TextEditingController();
                usernameInput.text = username ?? '';
                await showModalBottomSheet(
                    context: context,
                    builder: (BuildContext context) {
                      return Padding(
                        padding: EdgeInsets.only(
                            top: 20,
                            left: 20,
                            right: 20,
                            bottom: MediaQuery.of(context).viewInsets.bottom),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            const Text("Definir nome de usuario"),
                            TextField(
                              decoration: const InputDecoration(
                                  labelText: 'Digite seu nome de usuario'),
                              controller: usernameInput,
                            ),
                            ElevatedButton(
                                onPressed: () async {
                                  if (usernameInput.text != '') {
                                    setState(() {
                                      username = usernameInput.text;
                                    });
                                  }
                                  Navigator.of(context, rootNavigator: true)
                                      .pop();
                                },
                                child: const Text('Salvar'))
                          ],
                        ),
                      );
                    });
              }),
          const SizedBox(
            width: 10,
          ),
          FloatingActionButton(
            heroTag: "btn2",
            child: const Icon(Icons.add),
            onPressed: () async {
              if (username == null) {
                alerta('Você deve estar logado para adicionar um comentário.');
              } else {
                var comentario = TextEditingController();
                await showModalBottomSheet(
                    context: context,
                    builder: (BuildContext context) {
                      return Padding(
                        padding: EdgeInsets.only(
                            top: 20,
                            left: 20,
                            right: 20,
                            bottom: MediaQuery.of(context).viewInsets.bottom),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            const Text("Publicar comentário"),
                            TextField(
                              decoration: const InputDecoration(
                                  labelText: 'Digite seu comentário'),
                              controller: comentario,
                            ),
                            ElevatedButton(
                                onPressed: () async {
                                  if (comentario.text != '') {
                                    var result = await comentarios.add({
                                      'conteudo': comentario.text,
                                      'upvotes': <int>[],
                                      "downvotes": <int>[],
                                      "usuario": username,
                                      "prova": id
                                    });
                                  }
                                  Navigator.of(context, rootNavigator: true)
                                      .pop();
                                },
                                child: const Text('Publicar'))
                          ],
                        ),
                      );
                    });
              }
            },
          ),
        ],
      ),
    );
  }

  alerta(String texto) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text("Alerta"),
            content: Text(texto),
            actions: [
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop();
                  },
                  child: const Text("Ok")),
            ],
          );
        });
  }
}
