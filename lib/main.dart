import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:em_formacao/Disciplina.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Flutter Firebase',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  CollectionReference disciplinas =
      FirebaseFirestore.instance.collection('disciplinas');

  var nomeController = TextEditingController();
  var periodoController = TextEditingController();
  String textoBusca = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Disciplinas")),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            TextField(
              decoration: const InputDecoration(
                  labelText: 'Procurar', suffixIcon: Icon(Icons.search)),
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              onChanged: (value) {
                setState(() {
                  textoBusca = value;
                });
              },
            ),
            const SizedBox(
              height: 20,
            ),
            Flexible(
              child: StreamBuilder(
                stream: disciplinas.snapshots(),
                builder: (BuildContext context, AsyncSnapshot streamSnapshot) {
                  if (streamSnapshot.hasData) {
                    List<DocumentSnapshot> documents = streamSnapshot.data.docs;
                    documents.sort((a, b) => a['periodo'].round() - b['periodo'].round());
                    if (textoBusca.isNotEmpty) {
                      documents = documents.where((element) {
                        return element
                            .get('nome')
                            .toString()
                            .toLowerCase()
                            .contains(textoBusca.toLowerCase());
                      }).toList();
                    }
                    return ListView.builder(
                      itemCount: documents.length,
                      itemBuilder: (context, index) {
                        final DocumentSnapshot disciplina = documents[index];
                        var periodo =
                            double.tryParse(disciplina['periodo'].toString())
                                    ?.round() ??
                                0;
                        return ListTile(
                          title: Text(disciplina['nome']),
                          subtitle: Text(
                              periodo == 0 ? "Eletiva" : '$periodoº Período'),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => Disciplina(
                                      id: disciplina.id,
                                      nome: disciplina['nome'],
                                    )));
                          },
                          trailing: SizedBox(
                            width: 100,
                            child: Row(
                              children: [
                                IconButton(
                                    onPressed: () {
                                      update(disciplina);
                                    },
                                    icon: Icon(Icons.edit)),
                                IconButton(
                                    onPressed: () {
                                      excluir(disciplina);
                                    },
                                    icon: Icon(Icons.delete)),
                              ],
                            ),
                          ),
                        );
                      },
                    );
                  } else {
                    return CircularProgressIndicator();
                  }
                },
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          create();
        },
      ),
    );
  }

  Future<void> update(DocumentSnapshot<Object?> disciplina) async {
    nomeController.text = disciplina['nome'];
    periodoController.text = disciplina['periodo'].round().toString();
    await showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(
                  top: 20,
                  left: 20,
                  right: 20,
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  const Text("Inserir Disciplina"),
                  TextField(
                    controller: nomeController,
                    decoration:
                        const InputDecoration(labelText: 'Nome da Disciplina'),
                  ),
                  TextField(
                    controller: periodoController,
                    decoration: const InputDecoration(
                        labelText: 'Período da Disciplina:'),
                    keyboardType:
                        const TextInputType.numberWithOptions(decimal: false),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                      onPressed: () async {
                        String nome = nomeController.text;
                        double? periodo =
                            double.tryParse(periodoController.text);

                        if (nome != null && periodo != null) {
                          await disciplinas
                              .doc(disciplina?.id)
                              .update({'nome': nome, 'periodo': periodo});
                        }
                      },
                      child: const Text('Alterar'))
                ],
              ),
            ),
          );
        });
  }

  Future<void> create() async {
    nomeController.text = '';
    periodoController.text = '';
    await showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(
                  top: 20,
                  left: 20,
                  right: 20,
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  const Text("Inserir Disciplina"),
                  TextField(
                    controller: nomeController,
                    decoration:
                        const InputDecoration(labelText: 'Nome da Disciplina'),
                  ),
                  TextField(
                    controller: periodoController,
                    decoration: const InputDecoration(
                        labelText: 'Período da Disciplina:'),
                    keyboardType:
                        const TextInputType.numberWithOptions(decimal: false),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                      onPressed: () async {
                        String nome = nomeController.text;
                        double? periodo =
                            double.tryParse(periodoController.text);

                        if (nome != null && periodo != null) {
                          await disciplinas
                              .add({'nome': nome, 'periodo': periodo});
                        }
                      },
                      child: const Text('Criar'))
                ],
              ),
            ),
          );
        });
  }

  excluir(DocumentSnapshot<Object?> disciplina) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text("Confirmação"),
            content: Text(
                'Tem Certeza que deseja excluir a disciplina ${disciplina['nome']}?'),
            actions: [
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop();
                  },
                  child: const Text("Cancelar")),
              ElevatedButton(
                  onPressed: () async {
                    await disciplinas.doc(disciplina.id).delete();
                    Navigator.of(context, rootNavigator: true).pop();
                  },
                  child: const Text("Confirmar"))
            ],
          );
        });
  }
}
