import 'package:em_formacao/Prova.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

class Disciplina extends StatefulWidget {
  final String id;
  final String nome;

  const Disciplina({super.key, required this.id, required this.nome});

  @override
  State<Disciplina> createState() => _DisciplinaState(this.id, this.nome);
}

class _DisciplinaState extends State<Disciplina> {
  final String id;
  final String nome;

  final storage = FirebaseStorage.instance;
  CollectionReference disciplinas =
      FirebaseFirestore.instance.collection('provas');

  FilePickerResult? arquivo;

  _DisciplinaState(this.id, this.nome);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(nome)),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              const SizedBox(
                height: 10,
              ),
              const Center(
                child: Text(
                  "Provas Disponíveis",
                  style: TextStyle(fontSize: 20),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Flexible(
                child: StreamBuilder(
                  stream: disciplinas.snapshots(),
                  builder:
                      (BuildContext context, AsyncSnapshot streamSnapshot) {
                    if (streamSnapshot.hasData) {
                      List<DocumentSnapshot> documents =
                          streamSnapshot.data.docs;
                      documents = documents.where((element) {
                        return element.get('disciplina').toString() == nome;
                      }).toList();
                      return ListView.builder(
                        itemCount: documents.length,
                        itemBuilder: (context, index) {
                          final DocumentSnapshot prova = documents[index];
                          return Card(
                            color: Colors.blue,
                            elevation: 10,
                            margin: const EdgeInsets.symmetric(vertical: 10),
                            child: ListTile(
                                leading: Text(prova['periodo']),
                                title: Text(prova['nome']),
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => Prova(
                                          id: prova.id,
                                          nome: prova['nome'],
                                          disciplina: nome)));
                                },
                                subtitle:
                                    Text('Professor(a) ${prova["professor"]}')),
                          );
                        },
                      );
                    } else {
                      return CircularProgressIndicator();
                    }
                  },
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
            child: const Icon(Icons.add),
            onPressed: () async {
              String periodo = '';
              String tipoProva = '';
              String professor = '';

              await showModalBottomSheet(
                  context: context,
                  builder: (BuildContext context) {
                    return SingleChildScrollView(
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: 20,
                            left: 20,
                            right: 20,
                            bottom: MediaQuery.of(context).viewInsets.bottom),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            const Text("Inserir Prova"),
                            TextField(
                              onChanged: (value) {
                                setState(() {
                                  periodo = value;
                                });
                              },
                              decoration: const InputDecoration(
                                  labelText: 'Período da prova'),
                            ),
                            TextField(
                              onChanged: (value) {
                                setState(() {
                                  tipoProva = value;
                                });
                              },
                              decoration: const InputDecoration(
                                  labelText: 'Tipo de prova'),
                            ),
                            TextField(
                              onChanged: (value) {
                                setState(() {
                                  professor = value;
                                });
                              },
                              decoration: const InputDecoration(
                                  labelText: 'Nome do professor'),
                            ),
                            ElevatedButton(
                                onPressed: () async {
                                  var novoArquivo = await FilePicker.platform
                                      .pickFiles(withData: true);
                                  setState(() {
                                    arquivo = novoArquivo;
                                  });
                                },
                                child: const Text('Escolher Arquivo')),
                            Text(arquivo?.files?.first?.name ?? ''),
                            const SizedBox(
                              height: 20,
                            ),
                            ElevatedButton(
                                onPressed: () async {
                                  if (arquivo != null) {
                                    var fileBytes =
                                        arquivo!.files.first.bytes;
                                    String fileName =
                                        arquivo!.files.first.name;

                                    if (fileBytes != null) {
                                      var result = await disciplinas.add({
                                        'disciplina': nome,
                                        'nome': tipoProva,
                                        "periodo": periodo,
                                        "professor": professor
                                      });

                                      await FirebaseStorage.instance
                                          .ref('provas/${result.id}.pdf')
                                          .putData(fileBytes!);

                                      Navigator.of(context,
                                              rootNavigator: true)
                                          .pop();
                                    } else {
                                      print(fileName);
                                    }
                                  }
                                },
                                child: const Text('Adicionar'))
                          ],
                        ),
                      ),
                    );
                  });
            }));
  }
}
